const mongoose = require('mongoose');

const productSchema = new mongoose.Schema({

	brandName: {
		type: String,
		require: [true, 'Brand Name is required']
	},
	productName: {
		type: String,
		require: [true, 'Product Name is required']
	},
	description: {
		type: String,
		require: [true, 'Product Description is required']
	},
	price: {
		type: Number,
		require: [true, 'Product Price is required']
	},
	isActive: {
		type: Boolean,
		default: true
	},
	createdOn: {
		type: Date,
		default: new Date()
	}

})

module.exports = mongoose.model("Product", productSchema)